<?php
include 'auth.php';
$callback = $_REQUEST['callback'];
$success = 'false';
$query="SELECT * from user" or die("Cannot access item");
$result = mysqli_query($conn, $query);
$output = array();
if(mysqli_num_rows($result) > 0){
	while($obj = mysqli_fetch_object($result)) {
		$output[] = $obj;
	}
	$success = 'true';
}

//start output
if ($callback) {
    header('Content-Type: text/javascript');
    echo $callback . '({"success":'.$success.', "items":' . json_encode($output) . '});';
} else {
    header('Content-Type: application/x-json');
    echo json_encode($output);
}
$conn->close();
?>

