Ext.define("Pertemuan.util.Globals",{
    singleton : true,
    alternateClassName: 'globalUtils',
    version:'1.0',
    config: {
	    phppath: 'https://wahyusafrizal23.000webhostapp.com/resources'
    },
    constructor : function(config) {
        this.initConfig(config);
    },

    startRecordCordova: function(){
	    let opts = {limit:1}
	    navigator.device.capture.captureAudio(globalUtils.captureSuccess, globalUtils.captureError, opts);
	},
	captureSuccess: function(mediaFiles){
	    var i, path, len;
	    name = mediaFiles[0].name;
	    fileURL = mediaFiles[0].fullPath;
        type = mediaFiles[0].type;
        size = mediaFiles[0].size;
        if(size>80000){
        	document.getElementById('recordingInfo').textContent = "Rekaman Anda terlalu panjang, silakan coba lagi.";        
        }
        else{
        	document.getElementById('recordingInfo').textContent = "Menyimpan rekaman suara...";   

        	// Proses upload ke server
        	var uri = encodeURI("https://wahyusafrizal23.000webhostapp.com/resources/php/upload.php");
			var options = new FileUploadOptions();
			options.fileKey="file";
	    	var filename = "rekaman.m4a";
			options.fileName=filename;
			options.mimeType="text/plain";
			var params = {};
			params.user = 'wahyusafrizal';
			params.npm = '183510512';
			options.params = params;
	      	var ft = new FileTransfer();
			ft.upload(fileURL, uri, globalUtils.uploadSuccess, globalUtils.uploadError, options);     
        }					  
	},
	captureError: function(error){
	    document.getElementById('recordingInfo').textContent = 'Error code: ' + error.code;
	},

	// Fungsi callback untuk proses upload
	uploadSuccess: function(r){
		if(r.response=="failed"){
	    	document.getElementById('recordingInfo').textContent = "Rekaman gagal disimpan.";	    	
	    }	    
	    else{	    	
	    	document.getElementById('recordingInfo').textContent = "Rekaman berhasil disimpan.";
	    }
	},
	uploadError: function(error){
		Ext.Msg.alert("An error has occurred: Code = " + error.code);	    
	}
});