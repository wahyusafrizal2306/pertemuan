Ext.define('Pertemuan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Ext.field.Search'
    ],

    viewModel: {
        stores:{
            personnel: {
                type: 'personnel'
            }
        }
    },
    layout: 'fit',
    cls: '',
    shadow: true,
    items: [{
        docked: 'top',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'searchfield',
                placeHolder: 'Search name',
                name: 'searchfield',
                listeners:{
                    change: function(me, newValue, oldValue, eOpts){
                    personnelStore = Ext.getStore('personnel');
                    personnelStore.filter('name',newValue);
                }
            }
            },{
                xtype: 'spacer'
            },
            {
                xtype: 'searchfield',
                placeHolder: 'Search npm',
                name: 'searchfield',
                listeners:{
                    change: function(me, newValue, oldValue, eOpts){
                    personnelStore = Ext.getStore('personnel');
                    personnelStore.filter('npm', newValue);
                }
            }
            },{
                xtype: 'spacer'
            },
            {
                xtype: 'searchfield',
                placeHolder: 'Search email',
                name: 'searchfield',
                listeners:{
                    change: function(me, newValue, oldValue, eOpts){
                    personnelStore = Ext.getStore('personnel');
                    personnelStore.filter('email', newValue);
                }
            }
            }
        ]
    },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<div class="img" style="float: left"> <img src="{photo}" width="100px" height="100px"></div><br><br><div class="content"><div><font size=4 color="blue"><b>{npm}</b></font><br><font color="red"><b>{name}</b></font><br><i>{email}<i><br><b>{phone}</b></div><div class="affiliation">{affiliation}</div></div> <hr>',
        bind: {
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>Npm: </td><td>{npm}</td></tr>' +
                    '<tr><td>Name:</td><td>{name}</td></tr>' + 
                    '<tr><td>E-mail:</td><td>{email}</td></tr>' + 
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' 
        }
    }]
});