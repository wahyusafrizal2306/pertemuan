Ext.define('Pertemuan.view.setting.DataTree', {
    extend: 'Ext.Container',
    xtype: 'datatree',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Ext.field.Search'
    ],

    viewModel: {
        stores:{
            personnel: {
                type: 'personnel'
            }
        }
    },
    layout: 'fit',
    cls: '',
    shadow: true,
    items: [{
        docked: 'top',
        xtype: 'toolbar',
        items: [
            {
                html: 'Data Mahasiswa Berdasarkan Hobi'
            }
        ]
    },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<div class="img" style="float: left"> <img src="resources/{photo}" width="100px" height="100px"></div><br><div class="content"><div><font size=4 color="blue"><b>{npm}</b></font><br><font color="red"><b>{name}</b></font><br><i>{email}<i><br><b>{phone}</b><br>{hobi}</div><div class="affiliation">{affiliation}</div></div><br> <hr>',
        bind: {
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>Npm: </td><td>{npm}</td></tr>' +
                    '<tr><td>Name:</td><td>{name}</td></tr>' + 
                    '<tr><td>E-mail:</td><td>{email}</td></tr>' + 
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' 
        }
    }]
});