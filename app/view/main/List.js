/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Pertemuan.store.Personnel'
    ],

    title: 'Data Mahasiswa Sencha',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Npm',  dataIndex: 'npm', width: 150 },
        { text: 'Nama',  dataIndex: 'name', width: 200 },
        { text: 'Email', dataIndex: 'email', width: 260 },
        { text: 'Telepon', dataIndex: 'phone', width: 150 },
        { text: 'Hobi', dataIndex: 'hobi', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
