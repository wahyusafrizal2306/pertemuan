/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Pertemuan.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        var nama = record.data.name;
        var npm = record.data.npm;
        localStorage.setItem('nama',nama);
        localStorage.setItem('npm',npm);
        Ext.Msg.confirm('Konfirmasi', 'Yakin '+nama+'?', 'onConfirm', this);
        console.log(record.data);
    },

    onConfirm: function (choice) {
        var nama = localStorage.getItem('nama');
        var npm = localStorage.getItem('npm');
        if (choice === 'yes') {
            alert ('Terimakasih memilih YES '+nama+' ('+npm+')');
        }
        else{
            alert ('Jangan memilih NO '+nama+' ('+npm+')');
        }
    }
});
